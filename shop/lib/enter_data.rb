require_relative 'search.rb'

# This module contains all methods for entering different types of data
module EnterData
  def self.enter_integer
    line = gets.chomp
    while line !~ /\d+/
      puts 'Введите целое число'
      line = gets.chomp
    end
    Integer(line)
  end

  def self.enter_float
    line = gets.chomp
    while line !~ /\d+.\d+|\d+/
      puts 'Введите вещественное число'
      line = gets.chomp
    end
    Float(line)
  end

  def self.enter_book_data(book_list)
    puts 'Введите название книги'
    name = gets.chomp
    puts 'Введите фамилию автора книги'
    author = gets.chomp
    index = Search.search_book_by_name_and_author(book_list, name, author)
    return EnterData.enter_second_part_book_data(name, author) if index.nil?
  end

  def self.enter_second_part_book_data(name, author)
    puts 'Введите жанр книги'
    genre = gets.chomp
    puts 'Введите цену книги'
    price = EnterData.enter_float
    puts 'Введите количество книг'
    [name, author, genre, price, EnterData.enter_integer]
  end
end
