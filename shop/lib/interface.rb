require_relative 'selections.rb'

# This module contains interface methods
module Interface
  def self.selection(option, item_list, book_list, buy_list)
    case option
    when '1'
      Selections.add_delete_selection(item_list, book_list)
    when '2'
      Selections.search_selection(item_list, book_list)
    when '3'
      Selections.show_all_selection(item_list, book_list)
    when '4'
      SubMethods.choose_item_to_buy(item_list, buy_list, book_list)
    when '5'
      SubMethods.show_buy_list_with_prices(buy_list)
    else
      puts 'Неверный ввод'
    end
  end

  def self.greeting
    puts 'Выберите действие:'
    puts '1. Добавить/удалить книгу или канц. принадлежность.'
    puts '2. Поиск товара/книги.'
    puts '3. Вывод списка всех доступных товаров и книг в упорядоченных по названию.'
    puts '4. Выбор товара для покупки.'
    puts '5. Вывод списка покупок.'
    puts '0. Покинуть магазин.'
  end
end
