require_relative 'item.rb'
# This class identifies all books objects
class Book < Item
  attr_reader :name, :author, :genre
  attr_accessor :count, :price
  def initialize(name, author, genre, price, count)
    @name = name
    @author = author
    @genre = genre
    @price = price
    @count = count
  end

  def to_s
    "#{@name}; Автор: #{@author}; Жанр: #{@genre}; Цена: #{@price} $; Кол-во: #{@count} шт."
  end
end
