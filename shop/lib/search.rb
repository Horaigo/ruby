# This module contains search methods
module Search
  def self.search_item_by_name(list, names)
    list.each_with_index do |item, index|
      return index if item.name == names
    end
    nil
  end

  def self.search_book_by_genre(book_list)
    puts 'Введите жанр книги'
    genres = gets.chomp
    book_list.each_with_index do |book, index|
      return index if book.genre == genres
    end
    nil
  end

  def self.search_book_by_name_and_author(book_list, names, authors)
    book_list.each_with_index do |book, index|
      return index if book.name == names && book.author == authors
    end
    nil
  end
end
