require_relative 'sub_methods.rb'
require_relative 'greetings.rb'

# This module contains all selection methods
module Selections
  def self.main_menu_selection(option, item_list, book_list, buy_list)
    case option
    when '1'
      Selections.add_delete_selection(item_list, book_list)
    when '2'
      Selections.search_selection(item_list, book_list)
    when '3'
      Selections.show_all_selection(item_list, book_list)
    when '4'
      SubMethods.choose_item_to_buy(item_list, buy_list, book_list)
    when '5'
      SubMethods.show_buy_list_with_prices(buy_list)
    else
      puts 'Неверный ввод'
    end
  end

  def self.add_delete_selection(item_list, book_list)
    option = Greetings.add_delete_greeting
    while option != '0'
      case option
      when '1'
        Selections.add_selection(item_list, book_list)
      when '2'
        Selections.delete_selection(item_list, book_list)
      else
        puts 'Неверный ввод'
      end
      option = Greetings.add_delete_greeting
    end
  end

  def self.add_selection(item_list, book_list)
    option = Greetings.add_greeting
    while option != '0'
      case option
      when '1'
        SubMethods.add_item(item_list)
      when '2'
        SubMethods.add_book(item_list, book_list)
      else
        puts 'Неверный ввод'
      end
      option = Greetings.add_greeting
    end
  end

  def self.delete_selection(item_list, book_list)
    option = Greetings.delete_greeting
    while option != '0'
      case option
      when '1'
        SubMethods.delete_item(item_list)
      when '2'
        SubMethods.delete_book(item_list, book_list)
      else
        puts 'Неверный ввод'
      end
      option = Greetings.delete_greeting
    end
  end

  def self.search_selection(item_list, book_list)
    option = Greetings.search_greeting
    while option != '0'
      case option
      when '1'
        Search.search_item_by_name(item_list, Greetings.find_item_by_name_greeting)
      when '2'
        Search.search_item_by_name(book_list, Greetings.find_book_by_name_greeting)
      when '3'
        Search.search_book_by_genre(book_list)
      else
        puts 'Неверный ввод'
      end
      option = Greetings.search_greeting
    end
  end

  def self.show_all_selection(item_list, book_list)
    option = Greetings.show_all_greeting
    while option != '0'
      case option
      when '1'
        SubMethods.show_items_ordered_by_name(item_list)
      when '2'
        SubMethods.show_books_ordered_by_name(book_list)
      else
        puts 'Неверный ввод'
      end
      option = Greetings.show_all_greeting
    end
  end
end
