# This module contains most of all greetings
module Greetings
  def self.main_menu_greeting
    puts 'Выберите действие:'
    puts '1. Добавить/удалить книгу или канц. принадлежность.'
    puts '2. Поиск товара/книги.'
    puts '3. Вывод списка всех доступных товаров и книг в упорядоченных по названию.'
    puts '4. Выбор товара для покупки.'
    puts '5. Вывод списка покупок.'
    puts '0. Покинуть магазин.'
  end

  def self.add_delete_greeting
    puts 'Выберите действие:'
    puts '1. Добавить товар/книгу.'
    puts '2. Удалить товар/книгу.'
    puts '0. Вернуться в предыдущее меню.'
    gets.chomp
  end

  def self.add_greeting
    puts 'Выберите действие:'
    puts '1. Добавить товар.'
    puts '2. Добавить книгу.'
    puts '0. Вернуться в предыдущее меню.'
    gets.chomp
  end

  def self.delete_greeting
    puts 'Выберите действие:'
    puts '1. Удалить товар.'
    puts '2. Удалить книгу.'
    puts '0. Вернуться в предыдущее меню.'
    gets.chomp
  end

  def self.search_greeting
    puts 'Выберите действие:'
    puts '1. Поиск товара по названию.'
    puts '2. Поиск книги по названию.'
    puts '3. Поиск книги по жанру.'
    puts '0. Вернуться в предыдущее меню.'
    gets.chomp
  end

  def self.show_all_greeting
    puts 'Выберите действие:'
    puts '1. Показать все товары упорядоченными по имени.'
    puts '2. Показать все книги упорядоченными по имени.'
    puts '0. Вернуться в предыдущее меню.'
    gets.chomp
  end

  def self.find_item_by_name_greeting
    puts 'Введите название товара, который необходимо найти'
    gets.chomp
  end

  def self.find_book_by_name_greeting
    puts 'Введите название книги, который необходимо найти'
    gets.chomp
  end
end
