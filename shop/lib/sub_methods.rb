require_relative 'enter_data.rb'
require_relative 'search.rb'
require_relative 'item.rb'
require_relative 'book.rb'

# This module contains all subsidiary methods
module SubMethods
  def self.add_item(item_list)
    puts 'Введите название товара'
    name = gets.chomp
    index = Search.search_item_by_name(item_list, name)
    if index.nil?
      puts 'Введите цену товара'
      price = EnterData.enter_float
      puts 'Введите количество товара'
      count = EnterData.enter_integer
      item_list.push(Item.new(name, price, count))
      puts 'Товар добавлен'
    else
      puts 'Товар с введенным названием уже существует'
    end
  end

  def self.add_book(item_list, book_list)
    book_data = EnterData.enter_book_data(book_list)
    if !book_data.nil?
      book = Book.new(book_data[0], book_data[1], book_data[2], book_data[3], book_data[4])
      item_list.push(book)
      book_list.push(book)
      puts 'Книга добавлена'
    else
      puts 'Книга данного автора с таким названием уже есть в ассортименте'
    end
  end

  def self.delete_item(item_list)
    puts 'Введите название товара, который будет удален'
    names = gets.chomp
    item_list.delete_if { |item| item.name == names }
    puts 'Товар удален'
  end

  def self.delete_book(item_list, book_list)
    puts 'Введите название книги, которая будет удалена'
    names = gets.chomp
    item_list.delete_if { |item| item.name == names && !item.nil? }
    book_list.delete_if { |book| book.name == names }
    puts 'Книга удалена'
  end

  def self.show_items_ordered_by_name(item_list)
    item_list.sort_by!(&:name)
    puts item_list
  end

  def self.show_books_ordered_by_name(book_list)
    book_list.sort_by!(&:name)
    puts book_list
  end

  def self.choose_item_to_buy(item_list, buy_list, book_list)
    puts 'Введите название товара, который вы хотите купить.'
    name = gets.chomp
    index = Search.search_item_by_name(item_list, name)
    if index.nil?
      puts 'Такого товара нет'
      return
    else
      puts 'Введите количество данного товара, которое вы хотите купить'
      counts = EnterData.enter_integer
      SubMethods.give_item(item_list, counts, index, buy_list, book_list)
    end
  end

  def self.give_item(item_list, counts, index, buy_list, book_list)
    if item_list[index].count > 0
      if item_list[index].count < counts
        puts 'Выбранного товара нет в заданном количестве'
      else
        indexes = [index, Search.search_item_by_name(book_list, item_list[index].name)]
        SubMethods.give_new_add_old(item_list, counts, buy_list, book_list, indexes)
      end
    else
      puts 'Выбранного товара нет в наличии'
    end
  end

  def self.give_new_add_old(item_list, counts, buy_list, book_list, indexes)
    k = Search.search_item_by_name(buy_list, item_list[indexes[0]].name)
    item = item_list[indexes[0]].clone
    item_list[indexes[0]].count -= counts
    book_list[indexes[1]].count -= counts if !indexes[1].nil?
    if !k.nil?
      buy_list[k].price = buy_list[k].price / buy_list[k].count
      buy_list[k].count += counts
      buy_list[k].price = buy_list[k].price * buy_list[k].count
    else
      buy_list.push(item)
      buy_list[buy_list.length - 1].count = counts
      buy_list[buy_list.length - 1].price = buy_list[buy_list.length - 1].price * buy_list[buy_list.length - 1].count
    end
  end

  def self.show_buy_list_with_prices(buy_list)
    puts 'Ваш список покупок:'
    puts buy_list
    end_price = 0
    buy_list.each { |value| end_price += value.price }
    puts "Итоговая сумма: #{end_price} $"
  end
end
