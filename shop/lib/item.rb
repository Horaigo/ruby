# This class identifies all items objects
class Item
  attr_reader :name
  attr_accessor :count, :price
  def initialize(name, price, count)
    @name = name
    @price = price
    @count = count
  end

  def to_s
    "#{@name}; Цена: #{@price} $; Кол-во: #{@count} шт."
  end
end
