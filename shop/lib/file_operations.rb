require 'yaml'
# This module contains methods for working with files
module FileOperations
  def self.read_from_file(filename)
    YAML.load_file(filename)
  end

  def self.write_to_file(list, filename)
    File.open(filename, 'w') { |file| file.puts(list.to_yaml) }
  end
end
