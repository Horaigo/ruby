require_relative '../lib/selections.rb'
require_relative '../lib/greetings.rb'
require_relative '../lib/file_operations.rb'

item_list = FileOperations.read_from_file('../items.yml')
book_list = FileOperations.read_from_file('../books.yml')
buy_list = []
puts 'Добро пожаловать в магазин книг и канцелярских товаров'
Greetings.main_menu_greeting
option = gets.chomp
while option != '0'
  Selections.main_menu_selection(option, item_list, book_list, buy_list)
  Greetings.main_menu_greeting
  option = gets.chomp
end
FileOperations.write_to_file(item_list, '../items.yml')
FileOperations.write_to_file(book_list, '../books.yml')
